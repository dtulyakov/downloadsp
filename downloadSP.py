#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import argparse
import requests

def writeToFile(name, text):
    out = open(name, "w")
    out.write(text)
    out.close()

def createParser ():
    parser = argparse.ArgumentParser()
    parser.add_argument ('n', nargs='?')
    parser.add_argument ('t', nargs='?')
    return parser

def downloadPlay (url, name, mask):
	r = requests.get(url)
	if r.status_code == 200:
		txt = r.text
		txt = txt.replace(namespace.n, mask);
		writeToFile(name, txt)
		print("Все хорошо, смотри файл " + name)
	elif r.status_code == 404:
		print("Неправильная серия или перевод")
	else:
		print("Нет доступа")

parser = createParser()
namespace = parser.parse_args()

url = 'http://87.118.122.204/hls/[n]/[n]_[t].m3u8'
file_name = namespace.n + '_' + namespace.t + '.m3u';
mask = 'http://87.118.122.204/hls/[n]/[n]'
mask = mask.replace('[n]', namespace.n)
url = url.replace('[n]', namespace.n)
url = url.replace('[t]', namespace.t)

downloadPlay(url, file_name, mask)